#from tkinter import *
import tkinter as tk
from tkinter import messagebox, Tk, Label, Button, Text, Scrollbar, Entry
from app_chat import bot_name, ChatBot

import logging
from datetime import datetime

BG_GRAY = "#ABB2B9"
BG_COLOR = "#17202A"
TEXT_COLOR = "#EAECEE"

FONT = "Helvetica 14"
FONT_BOLD = "Helvetica 13 bold"


class ChatApplication:

    def __init__(self):
        self.window = Tk()
        self._setup_main_window()
        self.messageArray = []
        self.chatStart = datetime.now()
        self.chatBot = ChatBot()

    def _on_closing(self):
        if messagebox.askokcancel("Quit", "Do you want to quit?"):
            self._helpfull_missing(None)

    def run(self):
        self.window.protocol("WM_DELETE_WINDOW", self._on_closing)
        self.window.mainloop()

    def _setup_main_window(self):
        self.window.title("Support Chatbot")
        self.window.resizable(width=False, height=False)
        self.window.configure(width=470, height=550, bg=BG_COLOR)

        ###### head label ######
        head_label = Label(self.window, bg=BG_COLOR, fg=TEXT_COLOR,
                           text="Welcome", font=FONT_BOLD, pady=10)
        head_label.place(relwidth=1)

        ###### end  button ######
        self.end_button = Button(head_label, text="End Chat", font=FONT_BOLD, width=20, bg=BG_GRAY,
                             command=lambda: self._on_end_pressed(None))
        self.end_button.place(relx=0.77, rely=0.008, relheight=0.9, relwidth=0.22)

        ###### tiny divider ######
        line = Label(self.window, width=450, bg=BG_GRAY)
        line.place(relwidth=1, rely=0.07, relheight=0.012)

        ###### text widget ######
        self.text_widget = Text(self.window, width=20, height=2,
                                bg=BG_COLOR, fg=TEXT_COLOR, font=FONT, padx=5, pady=5)
        self.text_widget.place(relheight=0.745, relwidth=1, rely=0.08)
        self.text_widget.configure(cursor="arrow", state=tk.DISABLED)

        ###### scroll bar ######
        scrollbar = Scrollbar(self.text_widget)
        scrollbar.place(relheight=1, relx=0.974)
        scrollbar.configure(command=self.text_widget.yview)

        ###### bottom label ######
        bottom_label = Label(self.window, bg=BG_GRAY, height=80)
        bottom_label.place(relwidth=1, rely=0.825)

        ###### message entry box ######
        self.msg_entry = Entry(bottom_label, bg="#2C3E50",
                               fg=TEXT_COLOR, font=FONT)
        self.msg_entry.place(relwidth=0.74, relheight=0.06,
                             rely=0.008, relx=0.011)
        self.msg_entry.focus()
        self.msg_entry.bind("<Return>", self._on_enter_pressed)

        ###### send button ######
        send_button = Button(bottom_label, text="Send", font=FONT_BOLD, width=20, bg=BG_GRAY,
                             command=lambda: self._on_enter_pressed(None))
        send_button.place(relx=0.77, rely=0.008, relheight=0.06, relwidth=0.22)     

    def _on_enter_pressed(self, event):
        msg = self.msg_entry.get()
        self._insert_message(msg, "You")

    def _insert_message(self, msg, sender):
        if not msg:
            return

        self.msg_entry.delete(0, tk.END)
        msg1 = f"{sender}: {msg}\n\n"
        self.text_widget.configure(state=tk.NORMAL)
        self.text_widget.insert(tk.END, msg1)
        self.text_widget.configure(state=tk.DISABLED)

        msg2 = f"{bot_name}: {self.chatBot.get_response(msg)}\n\n"
        self.text_widget.configure(state=tk.NORMAL)
        self.text_widget.insert(tk.END, msg2)
        self.text_widget.configure(state=tk.DISABLED)

        self.text_widget.see(tk.END)

        messageArray = self.messageArray
        messageArray.append(msg1)
        messageArray.append(msg2)
        self.messageArray = messageArray
    
    def _on_end_pressed(self, event):
        self.end_button.destroy()
        end_button_label = Label(self.window, bg=BG_GRAY, height=80)
                           #text="Sam: War ich hilfreich?", font=FONT_BOLD, pady=10, height=80)
        end_button_label.place(relwidth=1, rely=0.825)

        end_text_widget = Text(end_button_label, width=20, height=2,
                                bg=BG_COLOR, fg=TEXT_COLOR, font=FONT, padx=5, pady=5)
        end_text_widget.place(relwidth=0.74, relheight=0.06, rely=0.008, relx=0.011)
        end_text_widget.configure(state=tk.DISABLED)

        msg1 = f"\nSam: War ich hilfreich?"
        end_text_widget.configure(state=tk.NORMAL)
        end_text_widget.insert(tk.END, msg1)
        end_text_widget.configure(state=tk.DISABLED)

        yes_button = Button(end_button_label, text="Yes", font=FONT_BOLD, width=20, bg=BG_GRAY,
                             command=lambda: self._helpfull_yes(None))
        yes_button.place(relx=0.77, rely=0.008, relheight=0.06, relwidth=0.11)

        no_button = Button(end_button_label, text="No", font=FONT_BOLD, width=20, bg=BG_GRAY,
                             command=lambda: self._helpfull_no(None))
        no_button.place(relx=0.88, rely=0.008, relheight=0.06, relwidth=0.11)
    
    def _helpfull_yes(self, event):
        logging.basicConfig(filename='src\\logs\\helpfull_yes.log', encoding='utf-8', level=logging.INFO, format='%(message)s')
        self._logging()
        self._end_chat()

    def _helpfull_no(self, event):
        logging.basicConfig(filename='src\\logs\\helpfull_no.log', encoding='utf-8', level=logging.INFO, format='%(message)s')
        self._logging()
        self._end_chat()

    def _helpfull_missing(self, event):
        logging.basicConfig(filename='src\\logs\\helpfull_missing.log', encoding='utf-8', level=logging.INFO, format='%(message)s')
        self._logging()
        self._end_chat()
    
    def _logging(self):
        if len(self.messageArray) == 0:
            return

        logging.info(f"Chat started at: {self.chatStart}\n")

        for message in self.messageArray: 
            message = message.replace("\n\n", "")          
            logging.info(message)

        logging.info(f"\nChat endet at: {datetime.now()}")
        logging.info("---------------------")

    def _end_chat(self):
        self.window.quit()


if __name__ == '__main__':
    app = ChatApplication()
    app.run()

import random
import json
import torch
from model import NeuralNet
from nltk_utils import bag_of_words, tokenize


bot_name = "Sam"
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class ChatBot():

    def __init__(self) -> None:
        self.load_model('base')

    def load_model(self, modelName):
        with open(f'src//trainingData/intents_{modelName}.json', "r") as f:
            self.intents = json.load(f)

        FILE = f"src/model/data_{modelName}.pth"
        data = torch.load(FILE)

        self.input_size = data['input_size']
        self.hidden_size = data['hidden_size']
        self.output_size = data['output_size']
        self.all_words = data['all_words']
        self.tags = data['tags']
        self.model_state = data['model_state']

        self.model = NeuralNet(
            self.input_size, self.hidden_size, self.output_size).to(device)
        self.model.load_state_dict(self.model_state)
        self.model.eval()

    def get_response(self, msg):
        sentence = tokenize(msg)
        x = bag_of_words(sentence, self.all_words)
        x = x.reshape(1, x.shape[0])
        x = torch.from_numpy(x)

        output = self.model(x)
        _, predicted = torch.max(output, dim=1)
        tag = self.tags[predicted.item()]

        probs = torch.softmax(output, dim=1)
        prob = probs[0][predicted.item()]
        if prob.item() > 0.75:
            for intent in self.intents["intents"]:
                if tag == intent["tag"]:
                    response = random.choice(intent["responses"])
                    match response:
                        case "software":
                            self.load_model(response)
                            response = self.get_response(msg)
                        case "hardware":
                            self.load_model(response)
                            response = self.get_response(msg)
                    return response

        return "Ich verstehe leider nicht"


if __name__ == "__main__":

    app = ChatBot()

    print("Start Chatting. Type quit to end the chat")
    while True:
        msg = input("You: ")
        if msg == 'quit':
            break

        print(f"{bot_name}: {app.get_response(msg)}")

from flask import Flask, render_template, request, jsonify

from app_chat import ChatBot

app = Flask(__name__)
chatBot = ChatBot()

@app.get("/")
def index_get():
    return render_template("base.html")


@app.post("/predict")
def predict():
    text = request.get_json().get("message")
    # TODO: check if text is valid
    response = chatBot.get_response(text)
    message = {"answer": response}
    return jsonify(message)


if __name__ == "__main__":
    app.run(debug=True)
